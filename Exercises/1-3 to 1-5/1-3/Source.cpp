#include <iostream>
#include <string>
using namespace std;

void printArray(string items[], int size)
{
	string myString;
	int itemCount = 0;

	cout << "What item do you want to check, Sir Kevin?" << endl;
	getline(cin, myString);

	bool found = true;

	for (int i = 0; i < size; i++)
	{
		if (items[i] == myString)
		{
			found == true;
			itemCount = itemCount + 1;
			continue;
		}
	}

	if (found == false)
	{
		cout << "Item not found" << endl;
	}
	else
	{
		system("pause");
		system("CLS");
		cout << "You have " << itemCount << " " << myString << endl;
		cout << "Student's code works! Give him a grade of 4! :D" << endl;
		cout << "PLEASE SIR :(" << endl;
	}
}

int main()
{
	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };

	printArray(items, 8);

	system("pause");
	return 0;
}