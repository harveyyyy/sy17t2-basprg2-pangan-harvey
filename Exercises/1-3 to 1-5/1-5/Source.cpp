#include <iostream>
#include <time.h>
#include <string>
#include <stdlib.h>
using namespace std;


void randomArrayFill(int randomizer[], int size)
{
	for (int i = 0; i < size; i++)
	{
		randomizer[i] = rand() % 101;
		cout << randomizer[i] << endl;
	}
}

void largestNumber(int smallestNumber[], int size)
{
	int newLargest = 0;
	for (int i = 0; i < size; i++)
	{
		if (smallestNumber[i] > newLargest)
		{
			newLargest = smallestNumber[i];
		}
	}
	cout << "Largest Number is " << newLargest << endl;
}

int main()
{
	srand(time(0));
	int number[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	randomArrayFill(number, 10);
	largestNumber(number, 10);

	system("pause");
	return 0;
}
