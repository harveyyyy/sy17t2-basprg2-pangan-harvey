#include <iostream>
using namespace std;

int factorial(int x)
{
	if (x > 0)
		return x * factorial(x - 1);
	else
		return 1;
}

int main()
{
	int factorial(int x);
	int x;
	cout << "Enter Integer: ";
	cin >> x;

	cout << "Factorial is " << factorial(x);
	cout << endl;
	system("pause");
	return 0;
}