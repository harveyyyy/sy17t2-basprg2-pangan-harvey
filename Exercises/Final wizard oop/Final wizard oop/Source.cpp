#include <iostream>
#include <string>
#include "Spell.h"
#include "Wizard.h"
using namespace std;

void printWizardStats(wizard *wizardNo)
{
	cout << "Name: " << wizardNo->wizardName;
	cout << "HP: " << wizardNo->hp;
	cout << "MP:" << wizardNo->mp;
}

int main()
{
	wizard *wizard1 = new wizard();
	wizard1->wizardName = "Gandalf";
	wizard1->hp = 100;
	wizard1->mp = 100;

	wizard *wizard2 = new wizard();
	wizard2->wizardName = "Dumbledore";
	wizard2->hp = 100;
	wizard2->mp = 100;

	spell *fireball = new spell();
	wizard1->currentSpell = fireball;
	spell *wizard2 = fireball;
	fireball->mpCost = 5;
	fireball->maxDamage = 15;
	fireball->minDamage = 0;

	do 
	{
		for (int i = 0; i < 1; i++)
		{
			printWizardStats(wizard1);
			printWizardStats(wizard2);

			wizard1->castSpell(*wizard2);
			system("pause");
			system("CLS");
		}

		for (int i = 0; i < 1; i++)
		{
			printWizardStats(wizard1);
			printWizardStats(wizard2);

			wizard2->castSpell(*wizard1);
			system("pause");
			system("CLS");
		}
	} while (wizard1->hp && wizard2->hp >= 0 || wizard1->mp && wizard2->mp > 0);

	if (wizard1->hp > wizard2->hp)
	{
		cout << "Gandalf has won the battle!" << endl;
		system("pause");
	}
	else if (wizard1->hp < wizard2->hp)
	{
		cout << "Dumbledore has won the battle!" << endl;
		system("pause");
	}
}