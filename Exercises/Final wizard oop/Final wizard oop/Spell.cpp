#include "Spell.h"
#include "Wizard.h"

void spell::use(wizard target)
{
	int randomDamage = (rand() % minDamage - maxDamage) + minDamage;

	target.hp -= randomDamage;
}
