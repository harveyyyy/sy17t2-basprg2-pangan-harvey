#pragma once
#include <string>
#include <iostream> 
using namespace std;

class wizard;

class spell
{
public:
	string spellName;
	int mpCost;
	int maxDamage = 15;
	int minDamage = 0;

	void use(wizard target);
};
