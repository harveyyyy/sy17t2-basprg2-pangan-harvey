#pragma once
#include "Spell.h"
#include <string>
#include <iostream> 
using namespace std;

class wizard
{
public:
	string wizardName;
	int hp;
	int mp;

	spell *currentSpell;

	void castSpell(wizard target);
};
