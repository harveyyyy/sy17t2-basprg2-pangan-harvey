#include <iostream>
#include <string>
#include <ctime>

using namespace std;

struct Roll
{
	string type;
	int rank[2];
	int value;
	int dice[2];
	int aidice[2];
};

///EX 2-1
void bet(int &money, int &bet)
{
	while (bet < 1 || bet > money)
	{
		cout << "Enter bet: ";
		cin >> bet;
		if (bet == 0)
		{
			cout << "Cannot bet 0 gold" << endl;
		}
	}
	money -= bet;
}

/// EX 2-3
void payout(Roll player, Roll dealer, int &bet, int &money)
{
	/// If the player wins
	if (player.rank[0] && player.rank[1] > dealer.rank[0] && dealer.rank[1])
	{
		money += bet * 2;
	}
	/// if player loses
	else if (player.rank[0] && player.rank[1] < dealer.rank[0] && dealer.rank[1])
	{	
		money -= bet * 2;
	}

}

///EX 2-4
/// 2-2 is here. dice roll at Roll Player, Dealer
void playRound(int &money)
{
	srand(time(0));
	int wager = 0;
	/// Call out wager function
	cout << "Your gold: " << money << endl;
	bet(money, wager);

	Roll player;
	player.rank[0] = rand() % 6 + 1;
	player.rank[1] = rand() % 6 + 1;
	Roll dealer;
	dealer.rank[0] = rand() % 6 + 1;
	dealer.rank[1] = rand() % 6 + 1;

	///Call payout function
	payout(player, dealer, wager, money);

	cout << endl;
	cout << "You rolled: " << player.rank[0] << "-" << player.rank[1] << endl;
	cout << "AI rolled: " << dealer.rank[0] << "-" << dealer.rank[1] << endl;
	cout << "Remaining gold: " << money << endl;
	cout << "Your bet: " << wager << endl;
	system("PAUSE");
	system("CLS");
	
}

int main()
{
	int money = 1000;
	int bet;
	int dice[2];
	int aidice[2];
	int size = 2;

	while (money > 0)
	{
		playRound(money);
	}

	system("pause");
	return 0;
}