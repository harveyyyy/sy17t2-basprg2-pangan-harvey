#pragma once
#include <string>
#include <vector>
using namespace std;

class skills
{
public:
	//data members
	string skillName;
	int skillDamage;
	int skillMP;

	// member functions
	int skillComp();
};