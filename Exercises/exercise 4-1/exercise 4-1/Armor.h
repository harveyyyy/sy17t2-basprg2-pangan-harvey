#pragma once
#include <string>
#include <vector>
using namespace std;

class armor
{
public:
	//data members
	string armorName;
	int armorDef;

	// member functions
	int armorComp();
};