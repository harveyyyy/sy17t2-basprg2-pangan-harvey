#pragma once
#include <string>
#include <vector>
using namespace std;

class weapon
{
public:
	//data members
	string weaponName;
	int weaponDmg;

	// member functions
	int weaponComp();
};