#pragma once
#include <string>
#include <iostream>
#include "Weapon.h"
#include "Accessories.h"
#include "Armor.h"
#include "Skills.h"
#include "Stats.h"
#include "Items.h"
using namespace std;

class player
{
public:
	//data members
	//idk if kasama pa yung playerMaxHp/playerMaxMP etc
	//HP, MP, EXP and LVL
	//also idk if dito dapat yung stats pero ginawa ko nalang header :D
	string playerName;
	int playerHp;
	int playerMp;
	int exp;
	int level;

	//member function
	string play();
	int hpComp();
	int mpComp();
	int expComp();
};
