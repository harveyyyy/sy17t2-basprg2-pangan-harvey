#pragma once
#include <string>
#include <iostream>
using namespace std;

class stats
{
public:
	//data members
	//basic stats
	int str;
	int dex;
	int vit;
	int magic;
	int spirit;
	int luck;

	// member functions
	//not sure about this part
	int atk();
	float atkPerc();
	int def();
	float defPerc();
	int magicAtk();
	int magicDef();
	float magicDefPerc();
};
	