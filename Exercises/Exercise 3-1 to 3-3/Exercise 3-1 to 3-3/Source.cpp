#include <iostream>
#include <string>
#include <time.h>
#include <cstdlib>
using namespace std;

// global dungeon fee since dungeon fee will not change
const int DUNGEON_FEE = 25;

// Exercise 3-1 (struct)
struct item
{
	string name;
	int gold;
};

// Exercise 3-1 (main function of item randomization)
// create item from 1 - 5 corresponding to item
item* createItem()
{
	item* newItem = new item;
	// randomizes 1 to 5 for items. 1 for mithril ore, 2 for sharp talon, and so on.
	int randomDraw = rand() % 5 + 1;

	if (randomDraw == 1)
	{
		newItem->name = "Mithril Ore";
		newItem->gold = 100;
	}

	else if (randomDraw == 2)
	{
		newItem->name = "Sharp Talon";
		newItem->gold = 50;
	}

	else if (randomDraw == 3)
	{
		newItem->name = "Thick Leather";
		newItem->gold = 25;
	}

	else if (randomDraw == 4)
	{
		newItem->name = "Jellopy";
		newItem->gold = 5;
	}

	else
	{
		newItem->name = "Cursed Stone";
		newItem->gold = 0;
	}

	return newItem;
}

//exercise 3-2
//looting
//if item is not cursedStone ask if player still wants to continue looting
//if yes add multiplier by 1
//if no add earnings to gold
void enterDungeon(int &gold)
{
	string choice;

	cout << "Do you wish to enter the dungeon? (yes/no)" << endl;
	cin >> choice;
	
	if (choice == "yes")
	{
		int earnings = 0;
		int multiplier = 1;

		gold -= DUNGEON_FEE;
		cout << DUNGEON_FEE << " was deducted from your gold." << endl;

		system("pause");
		system("cls");

		while(true)
		{
			cout << "Gold remaining: " << gold << endl;
			cout << "Earnings: " << earnings << endl;
			cout << "Looting..." << endl;

			srand(time(0));

			item* newItem = createItem();

			if (newItem->name == "Cursed Stone")
			{
				earnings = 0;

				cout << "Oh no! You have looted the Cursed Stone." << endl;
				cout << "You died." << endl;

				system("pause");
				system("cls");

				break;
			}

			else
			{
				cout << "You looted " << newItem->name << "." << endl;
				cout << "You earned " << newItem->gold*multiplier << "!" << endl;

				earnings += newItem->gold*multiplier;

				system("pause");
				system("cls");
			}

			string keepLooting;

			cout << "Gold remaining: " << gold << endl;
			cout << "Earnings: " << earnings << endl;
			cout << "Continue looting? yes/no" << endl;
			cin >> keepLooting;

			if (keepLooting == "yes")
			{
				multiplier++;

				system("cls");
			}

			else if (keepLooting == "no")
			{
				cout << earnings << " has been added to your gold." << endl;
				gold += earnings;

				system("pause");
				system("cls");

				break;
			}

			delete newItem;
		}
	}

	else if (choice == "no")
	{
		cout << "Goodbye!" << endl;

		system("pause");
		system("cls");
	}
}

//Exercise 3-3
// loop till gold is 0 or above 500
int main()
{
	int gold = 50;

	do
	{
		enterDungeon(gold);
	} while (gold >= 25 || gold < 500);

	if (gold <= 500)
		cout << "Congratulations! You collected 500 gold!";

	else if (gold < 25)
		cout << "Insufficient gold";

	system("pause");
	return 0;
}