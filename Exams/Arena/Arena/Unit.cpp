#include "Unit.h"
#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

Unit::Unit()
{
}

Unit::Unit(string name, string userClass)
{
	mName = name;
	mClass = userClass;
	mHp = 25;
	mMaxHp = 25;
	mPow = rand() % 10 + 1;
	mVit = rand() % 10 + 1;
	mAgi = rand() % 10 + 1;
	mDex = rand() % 10 + 1;

}

Unit::~Unit()
{
}

string Unit::getName()
{
	return mName;
}

string Unit::getClass()
{
	return mClass;
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getMaxHp()
{
	return mMaxHp;
}

int Unit::getPow()
{
	return mPow;
}

int Unit::getVit()
{
	return mVit;
}

int Unit::getAgi()
{
	return mAgi;
}

int Unit::getDex()
{
	return mDex;
}

void Unit::printStats()
{
	cout << "Name: " << mName << endl;
	cout << "Hp: " << mHp << "/" << mMaxHp << endl;
	cout << "Power: " << mPow << endl;
	cout << "Vitality: " << mVit << endl;
	cout << "Agility: " << mAgi << endl;
	cout << "Dexterity: " << mDex << endl;

}

void Unit::takeDamage(int damage)
{
	if (damage < 0)
	{
		return;
	}

	mHp -= damage;

	if (mHp < 0)
	{
		mHp = 0;
	}
}

void Unit::recoverHealth()
{
	mHp += (mMaxHp *.3);
	if (mHp >= mMaxHp)
	{
		mHp = mMaxHp;
	}
}

void Unit::addStat(string eClass)
{
	if (eClass == "Warrior")
	{
		mMaxHp += 3;
		mVit += 3;
	}
	else if (eClass == "Assassin")
	{
		mAgi += 3;
		mDex += 3;
	}
	else if (eClass == "Mage")
	{
		mPow += 5;
	}
	
}

bool Unit::faster(int pAgi, int eAgi)
{
	if (pAgi > eAgi)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Unit::hitRate(int attDex, int defAgi)
{
	int randomizeHit = rand() % 100 + 1;
	int hitRate = (attDex / defAgi) * 100;

	if (hitRate <= 20)
	{
		hitRate = 20;
	}
	else if (hitRate >= 80)
	{
		hitRate = 80;
	}

	if (hitRate >= randomizeHit)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int Unit::computeDamage(string attClass, string defClass)
{
	float bonusDamage = 0;

	if (attClass == "Warrior" && defClass == "Assassin"
		|| attClass == "Assassin" && defClass == "Mage"
		|| attClass == "Mage" && defClass == "Warrior")
	{
		bonusDamage = 2.0f;
	}
	else if (attClass == defClass)
	{
		bonusDamage = 1.0f;
	}
	else if (attClass == "Warrior" && defClass == "Mage"
		|| attClass == "Assassin" && defClass == "Warrior"
		|| attClass == "Mage" && defClass == "Assassin")
	{
		bonusDamage = 0.5;
	}

	//gave up lol
	//return attacker->getPow() - defender->getVit();
	return 5;
}
